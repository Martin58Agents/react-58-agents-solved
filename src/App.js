// Import CSS files for styling.
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';

// Import the SpaceCenterComponent to include it in the app.
import SpaceCenterComponent from "./components/SpaceCenterComponent";

// Define the main App component.
function App() {
  return (
    <div className="App">
      {/* Render the SpaceCenterComponent. */}
      <SpaceCenterComponent path="/"></SpaceCenterComponent>
    </div>
  );
}

// Export the App component as the entry point of the application.
export default App;
