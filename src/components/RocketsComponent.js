import { Button } from "react-bootstrap";
import RocketDetailsComponent from "./RocketDetailsComponent";
import React from "react";

function RocketsComponent({ fetchRockets, rockets, scheduleLaunch }) {
  // Function to render the "Load Rockets" button when there are no rockets.
  function renderLoadRocketsButton() {
    return (
      <Button onClick={fetchRockets}>Load Rockets</Button>
    );
  }

  return (
    <div className="rockets-container">
      <h1>Rockets</h1>
      {/* Render the "Load Rockets" button when there are no rockets. */}
      {!rockets.length && renderLoadRocketsButton()}

      <div>
        {/* Render the RocketDetailsComponent with rockets and scheduleLaunch prop. */}
        <RocketDetailsComponent
          scheduleLaunch={scheduleLaunch}
          rockets={rockets}
        />
      </div>
    </div>
  );
}

export default RocketsComponent;
