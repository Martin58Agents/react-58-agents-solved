import { Button } from "react-bootstrap";
import React from "react";
import "./LaunchesComponent.css";

function LaunchesComponent({ launches, removeLaunch }) {
  // Function to display a message when there are no launches scheduled.
  function renderNoLaunchesMessage() {
    return (
      <p>No launches scheduled yet</p>
    );
  }

  // Create an array of rendered launches using map.
  const renderLaunches = launches.map((launch) => (
    <li key={launch.id}>
      <Button className="mr-2" size="sm" onClick={() => removeLaunch(launch.id)}>
        X
      </Button>
      {launch.name} | Weight: {launch.mass.kg} kg
    </li>
  ));

  return (
    <div className="launches-container">
      <h1>Launches</h1>
      {/* Render the "No launches scheduled yet" message when there are no launches. */}
      {!launches.length && renderNoLaunchesMessage()}

      <ol>
        {/* Map over the launches and render each one as a list item. */}
        {renderLaunches}
      </ol>
    </div>
  );
}

export default LaunchesComponent;
