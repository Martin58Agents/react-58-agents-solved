import React, { useState, useEffect } from "react";
import LaunchesComponent from "./LaunchesComponent";
import RocketsComponent from "./RocketsComponent";

const SPACE_API_URL = "https://api.spacexdata.com/v4/rockets";

const SpaceCenterComponent = () => {
  // Initialize state for rockets and launches using the useState hook.
  const [rockets, setRockets] = useState([]);
  const [launches, setLaunches] = useState([]);

  // Fetch rockets data from the SpaceX API.
  const fetchRockets = async () => {
    try {
      const response = await fetch(SPACE_API_URL);
      const rocketsData = await response.json();
      return rocketsData;
    } catch (error) {
      console.error("Error fetching rockets:", error);
      return [];
    }
  };

  // Delete a specific launch by its id.
  const removeLaunch = (rocketId) => {
    setLaunches((prevLaunches) =>
      prevLaunches.filter((launch) => launch.id !== rocketId)
    );
  };

  // Asynchronously fetch rockets when the component mounts or when necessary.
  const getRockets = async () => {
    const rocketsData = await fetchRockets();
    setRockets(rocketsData);
    // Now you can do something with the response data, such as setting it in the component's state.
  };

  // Schedule a launch for a rocket if it's not already scheduled.
  const scheduleLaunch = (rocket) => {
    if (!launches.some((launch) => launch.id === rocket.id)) {
      // Add the rocket to the launches array in the component's state.
      setLaunches((prevLaunches) => [...prevLaunches, rocket]);
    }
  };

  // Use the useEffect hook to fetch rockets when the component mounts.
  /*
  useEffect(() => {
    getRockets();
  }, []);
  */

  return (
    <div>
      {/* Render the LaunchesComponent and RocketsComponent */}
      <LaunchesComponent launches={launches} removeLaunch={removeLaunch} />
      <RocketsComponent
        rockets={rockets}
        fetchRockets={getRockets}
        scheduleLaunch={scheduleLaunch}
      />
    </div>
  );
};

export default SpaceCenterComponent;
