import { Button, Card, ListGroup } from "react-bootstrap";
import React from "react";

function RocketDetailsComponent({ rockets, scheduleLaunch }) {
  // Create an array of rendered rockets using map.
  const renderRockets = rockets.map((rocket) => {
    const { mass, name, description, id } = rocket;
    return (
      <div key={id}>
        <Card style={{ width: "46rem" }} className="mt-4">
          {/* Use the current rocket's properties for rendering. */}
          <Card.Header>{name}</Card.Header>
          <ListGroup variant="flush">
            <ListGroup.Item className="rocketdetails">
              {description}
            </ListGroup.Item>
            <ListGroup.Item>Weight: {mass.kg} KG</ListGroup.Item>
          </ListGroup>
          {/* Pass the current rocket to the scheduleLaunch function. */}
          <Button onClick={() => scheduleLaunch(rocket)}>
            Schedule Launch
          </Button>
        </Card>
      </div>
    );
  });

  return (
    <div>
      {/* Map over the rockets and render details for each one. */}
      {renderRockets}
    </div>
  );
}

export default RocketDetailsComponent;
